# ModernGrid CLI

The ModernGrid CLI allows exporting and deploying ModernGrid lists and settings from your Salesforce org.
It is available as a Node.js module.
